//
//  RecipeViewController.swift
//  SwiftRecipe
//
//  Created by Kelson Vella on 9/27/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit

class RecipeViewController: UIViewController {
    var info: Dictionary<String, String>?
    
    @IBOutlet var recipeTitle: UILabel!
    @IBOutlet var recipeImage: UIImageView!
    
    @IBOutlet var recipeIngredients: UITextView!
    @IBOutlet var recipeInstructions: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        recipeInstructions.setContentOffset(CGPoint.zero, animated: false)
        recipeIngredients.setContentOffset(CGPoint.zero, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        recipeTitle.text = info?["recipeName"]
        recipeIngredients.text = info?["ingredients"]
        recipeInstructions.text = info?["directions"]
        let url = URL(string: (info?["image"])!)
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                self.recipeImage.image = UIImage(data: data!)
            }
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
