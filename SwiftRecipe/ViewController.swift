//
//  ViewController.swift
//  SwiftRecipe
//
//  Created by Kelson Vella on 9/27/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    var cells: NSArray?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (cells?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let obj = cells?.object(at: indexPath.row) as! Dictionary<String, String>
        cell.textLabel?.text = obj["recipeName"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let obj = cells?.object(at: indexPath.row) as! Dictionary<String, String>
        performSegue(withIdentifier: "move", sender: obj)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let path = Bundle.main.path(forResource: "recipes", ofType: "plist"){
            cells = NSArray(contentsOfFile: path)
        }
        print(cells as Any)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextViewController = segue.destination as! RecipeViewController
        nextViewController.info = sender as? Dictionary<String, String>
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

